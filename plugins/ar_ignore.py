#!/usr/bin/env python3

from argon.command import Command
from argon.matcher import RegexMatcher

ignore = [
    # A command for Tara, another bot in #pepper&carrot
    "!adduser"
]


def always(val):
    def func(*args, **kwargs):
        return val
    return func


def register_with(argon):
    argon.add_commands(*(
        Command(RegexMatcher(cmd + "( .*)?"),
                "", "",
                always(None),
                display_condition = always(False)
                )
        for cmd in ignore
    ))
