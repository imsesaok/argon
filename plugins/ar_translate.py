from mtranslate import translate
from argon.command import Command
from argon.matcher import RegexMatcher


def query(match, message, bot):
    original_text = match.group('string')
    dest_lang = match.group('dest')
    bot.reply(translate(original_text, dest_lang, "auto"), message)


def register_with(argon):
    argon.add_commands(
        # Instant Answers
        Command(RegexMatcher(r">(?P<dest>\w\w\w?) (?P<string>.+)"), "><destination language> <text>",
                "Translate a sentence into specified language using Google Translate.", query),
    )
