#!/usr/bin/env python3

from random import choice
from argon.command import Command
from argon.matcher import RegexMatcher

TOLERANCE = 6

chat_games = {}

with open('./hangman_words.txt', 'r') as _word_list_file:
    WORD_LIST = tuple(
        map(lambda x: x.rstrip('\n').lower(), _word_list_file.readlines())
    )
with open('./words_alpha.txt', 'r') as _word_list_file:
    VALID_GUESS_LIST = tuple(
        map(lambda x: x.rstrip('\n').lower(), _word_list_file.readlines())
    )


def chat_get_game(_match, message, _bot):
    adapter_id = message.adapter["_id"]
    channel = message.group

    if adapter_id not in chat_games:
        chat_games[adapter_id] = {}
        return None

    if channel not in chat_games[adapter_id]:
        return None

    return chat_games[adapter_id][channel]


def chat_new_game(_match, message, _bot):
    adapter_id = message.adapter["_id"]
    channel = message.group
    #hard = _match["hard"]
    
    if adapter_id not in chat_games:
        chat_games[adapter_id] = {}

    if _match.group('len'):
        word_len = int(_match["len_val"])
        filtered_word_list = [word for word in WORD_LIST if len(word) == word_len]
        
        if len(filtered_word_list) < TOLERANCE:
            _bot.send("Sorry, there is not enough {length}-letter words to run the game. Try a different number.".format(length=word_len),
                 channel, adapter_id)
            
            return
        
        word = choice(filtered_word_list)
    else:
        word = choice(WORD_LIST)
        word_len = len(word)

    game = {
        "hard": False,
        "word_len": word_len,
        "patterns": list(),
        "hitmap": set(),
        "included_chars": set(),
        "excluded_chars": set(),
        "previous_words": set(),
        "misses_left": TOLERANCE,
        "word": word,
    }

    chat_games[adapter_id][channel] = game
    return game


def chat_end(_match, message, _bot):
    adapter_id = message.adapter["_id"]
    channel = message.group

    if channel in chat_games.get(adapter_id, {}):
        del chat_games[adapter_id][channel]


def chat_start(match, message, bot):
    game = chat_get_game(match, message, bot)

    if game:
        bot.send("Send a message consisting of {length}-letter dictionary word to make a guess. "
             "To give up and start a new game, send '!wordle giveup' first.".format(length=game["word_len"]),
             message.group, message.adapter['_id'])
    
    else:
        game = chat_new_game(match, message, bot)
        
        if game:
            bot.reply(
                "Started a new wordle game in English! The word is {word_len} letters long. "
                "If you haven't played Wordle before, send '!help wordle' and learn the rules before playing. "
                "Guess a word (type the word with or without '!guess')…".format(word_len=game["word_len"]),
                message
            )
        
        chat_status(match, message, bot)

def chat_giveup(match, message, bot):
    game = chat_get_game(match, message, bot)
    
    if game:
        chat_status(match, message, bot)
        chat_end(match, message, bot)
        word = game["word"] #if not game["hard"] else matching_word.upper()
        bot.reply("Game over! The word was {}.".format(word), message)
    
    else:
        bot.reply("There's no wordle game going on here! Start one first by sending '!wordle'",
                  message)

def pattern_to_emoji(pattern):
    return "".join(
        "🟩" if c_index > 0 else "🟨" if c_index < 0 else "⬛️"
        for c_guess, c_index in pattern
    )

def chat_guess(match, message, bot):
    game = chat_get_game(match, message, bot)

    if not game:
        if match.group('cmd'):
            bot.reply("There's no wordle game going on here! Start one first by doing !wordle",
                      message)
        return

    guessed_word = match.group("word").lower()
    
    if len(guessed_word) != game["word_len"]:
        if match.group('cmd'):
            bot.reply("The length of your word ({guess}) must match the word you're guessing ({answer})".format(guess=len(guessed_word), answer=game["word_len"]),
                      message)
        return
    
    if guessed_word not in VALID_GUESS_LIST and guessed_word not in WORD_LIST:
        bot.reply("Your guess is not in our dictionary. Please try another word.", message)
        return

    if guessed_word in game["previous_words"]:
        bot.reply("You already tried {word}".format(word=guessed_word.upper()),
                  message)
        return
    
    current_pattern = list()
    
    for c_index, c_guess in enumerate(guessed_word):
        if c_guess == game["word"][c_index]:
            current_pattern.append((c_guess, c_index+1))
            game["included_chars"].add(c_guess)
        elif c_guess in game["word"]:
            current_pattern.append((c_guess, -c_index-1))
            game["included_chars"].add(c_guess)
        else:
            current_pattern.append((c_guess, 0))
            game["excluded_chars"].add(c_guess)

    #if game["hard"]:
    #    selection = select(game["pattern"], game["previous_letters"], guessed_word)
    #    if selection is None:
    #        bot.reply("Sorry, something went wrong!", message)
    #        chat_end(match, message, bot)
    #        return
    #    game["pattern"], matching_word = selection
    #else:
    
    
    game["patterns"].append(pattern_to_emoji(current_pattern))
    for c_hit in current_pattern:
        game["hitmap"].add(c_hit)

    game["previous_words"].add(guessed_word)

    if guessed_word == game["word"]:
        # Guessed the word!
        chat_status(match, message, bot)
        chat_end(match, message, bot)
        bot.reply("Congratulations, you guessed the word!", message)
        return
    else:
        game["misses_left"] -= 1
    
    if not game["misses_left"]:
        chat_status(match, message, bot)
        chat_end(match, message, bot)
        word = game["word"] #if not game["hard"] else matching_word.upper()
        bot.reply("Game over! The word was {}.".format(word), message)
        return

    chat_status(match, message, bot)


def chat_status(match, message, bot):
    game = chat_get_game(match, message, bot)
    
    if len(game["patterns"]) > 0:
        last_pattern = game["patterns"][-1]
        c_in = " ".join(sorted(game["included_chars"]))
        c_out = " ".join(sorted(game["excluded_chars"]))
        bot.send("{last_pattern} \n"
                 "In: {c_in} Out: {c_out}".format(last_pattern=last_pattern,
                                                c_in=c_in,
                                                c_out=c_out
                ),
             message.group, message.adapter['_id']
             )
     
    bot.send('{left}/{total} tries'.format(left=game["misses_left"],
                                     total=TOLERANCE
                                     ),
             message.group, message.adapter['_id']
             )


def register_with(argon):
    argon.add_commands(
        Command(RegexMatcher(r"!!wordle(?: (?P<len>-l ?|--length )(?P<len_val>[0-9]{1,3}))?"),
                "wordle (-l|--length <length of the word>)|wordle giveup",
                """Play a game of wordle: guess a word with words.
The word has to be in the dictionary and be the same length as the word to guess.
Results are displayed in square emojis;
⬛️(black square) indicates that the letter is not part of the word,
🟨(yellow square) indicates that the letter should be in a different position,
and 🟩(green square) indicates the correct guess of the position of the letter.
In the status, 'in' lists the letter included (yellow and green), and 'out' lists letters not included (black).
Player must guess the word correctly in 6 tries or less.
-l|--length <length of the word> use this option to specify the length of the word
wordle giveup: give up ongoing game""",
                chat_start
                ),
        Command(RegexMatcher(r"!!wordle giveup"),
                "", "",
                chat_giveup,
                display_condition=lambda match, message, bot: False,
                ),
        Command(RegexMatcher(r"(?P<cmd>!!guess )?(?P<word>[A-Za-z]+)"),
                "", "",
                chat_guess,
                display_condition=lambda match, message, bot: False,
                ),
    )
