from argon.command import Command
from argon.matcher import TypeMatcher, RegexMatcher
from argon.type import MessageType
from argon.adapter.discord import DiscordAdapter
import asyncio
from discord import FFmpegPCMAudio, PCMVolumeTransformer


## This feature is designed for Discord adapter and is currently Discord only.
def check_discord(match, message, bot):
    return message.adapter["adapter_type"] == DiscordAdapter.__name__

def connect_vc(guild, vc_index):
    vc = guild.voice_channels[vc_index]

    is_switch = guild.voice_client

    if not guild.voice_client:
        asyncio.create_task(vc.connect())

    else:
        asyncio.create_task(guild.voice_client.move_to(vc))

    return vc.name, is_switch

def join_vc(match, message, bot):
    ident = message.adapter["_id"]
    client = bot.adapters[ident].client

    guild = message.group.guild

    if not guild:
        bot.reply("This feature is only available in public server.", message)

    vc_count = len(guild.voice_channels)

    try:
        if match["room"]:
            index = int(match["room"]) - 1
        else:
            index = 0
    except ValueError:
        bot.reply("Please put an index number of the VC (1 to {}).".format(vc_count), message)

    if vc_count < 1:
        bot.reply("No VC (Voice Channel) available in this server.", message)
    elif index < vc_count:
        vc_name, is_switch = connect_vc(guild, index)
        bot.reply("{} to: {}".format("Switched" if is_switch else "Joined", vc_name), message)

    else:
        bot.reply("Index number should be from 1 to {}.".format(vc_count), message)

def part_vc(match, message, bot):
    guild = message.group.guild

    if guild.voice_client:
        asyncio.create_task(guild.voice_client.disconnect())
        bot.reply("Disconnecting...", message)
    else:
        bot.reply("Not connected to VC.", message)

file_matcher = TypeMatcher([MessageType.BINARY])

def play_music(match, message, bot):
    guild = message.group.guild

    location = file_matcher.match(message, bot)
    if not match["location"] and not location:
            bot.reply("Please specify the file location.", message)
            return
    elif match["location"]:
        location = match["location"]
    else:
        location = location[0]

    music = PCMVolumeTransformer(FFmpegPCMAudio(location, options='-vn'))

    after_play = lambda e: bot.send('Player error: %s' % e, message.group, message.adapter["_id"]) if e else bot.send("Playing concluded.", message.group, message.adapter["_id"])

    vc_client = guild.voice_client
    if vc_client:
        vc_client.stop()
        vc_client.play(music, after=after_play)

        bot.reply("Now playing...", message)

    else:
        bot.reply("Please invite the bot to VC with !joinvc first.", message)

def stop_music(match, message, bot):
    guild = message.group.guild
    vc_client = guild.voice_client
    if vc_client:
        vc_client.stop()
        bot.reply("Playback stopped.", message)
    else:
        bot.reply("Not connected to VC.", message)


def register_with(argon):
    argon.add_commands(
        # Join VC Channel
        Command(RegexMatcher(r"(?P<ident>!!)joinvc( (?P<room>.+))?"),
                "joinvc <VC channel index>",
                "Join VC chat", join_vc,
                display_condition=check_discord, exec_condition=check_discord),
        # Play music to channel
        Command(RegexMatcher(r"(?P<ident>!!)playmusic( (?P<location>.+))?"),
                "playmusic <URL to file>",
                "Play music (or FFmpeg-compatible file containing audio) in the link.",
                play_music,
                display_condition=check_discord, exec_condition=check_discord),
        # Stop music in channel
        Command(RegexMatcher(r"(?P<ident>!!)stopmusic"),
                "stopmusic",
                "Stop playing music in VC.",
                stop_music,
                display_condition=check_discord, exec_condition=check_discord),
        # Disconnect
        Command(RegexMatcher(r"(?P<ident>!!)partvc"),
                "partvc",
                "Disconnect from VC chat", part_vc,
                display_condition=check_discord, exec_condition=check_discord),
    )
