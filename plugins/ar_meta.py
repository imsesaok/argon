#!/usr/bin/env python3

import time
import re
from datetime import timedelta
from argon.command import Command, CannedResponseCommand
from argon.matcher import RegexMatcher, StringMatcher
from argon.adapter.telegram import TelegramAdapter

start_time = time.time()


def display_paginated(_list, index):
    linecount = 4
    list_length = len(_list)
    start_index = index * linecount
    end_index = start_index + linecount

    if index < 0:
        raise IndexError("Invalid number: use a number above or equal to 1.")

    maximum = int(-(-list_length // linecount))
    if list_length < end_index:
        end_index = list_length
        if end_index <= start_index:
            raise IndexError(
                "Invalid number: use a number below or equal to " + str(maximum) + ".")

    reply = ""
    for i in range(start_index, end_index):
        command = _list[i]
        reply += "\n• " + command.title + ": " + command.description.split("\n")[0]

    return maximum, reply


def print_help(match, message, bot):
    command_list = tuple(x for x in bot.commands
                         if x.display_condition(match, message, bot) and x.title)
    target_cmd = ""
    explicit_page = False

    args = match.groupdict()
    if args["page"]:
        index = int(args['page'].strip()) - 1
        explicit_page = True
    elif args["command"]:
        target_cmd = args['command']
    else:
        index = 0

    if target_cmd:
        for command in list(command_list):
            if target_cmd in command.title:
                bot.reply(command.title + ": " + command.description, message)
                return
        bot.reply("Command '{}' does not exist or is not available.".format(target_cmd), message)
    else:
        try:
            maximum, list_message = display_paginated(sorted(command_list, key=lambda x: x.title), index)

            cmd_count = len(command_list)
            reply = (
                "Usage: <identifier><command> (current <identifier>{regex_note}: `{ident}`)\n"
                "Total of {cmd_count} command{s} {are} available. Showing page {cur_page} out of {max_page}. Use "
                "`help <page>` for other pages. "
                if not explicit_page else
                "Page {cur_page} out of {max_page}"
            ).format(
                ident=message.adapter["ident"],
                regex_note=" is regex" if any(c in message.adapter["ident"] for c in ".*+?\\{}[]()^$") else "",
                cmd_count=cmd_count,
                s="s" if cmd_count > 1 else "",
                are="are" if cmd_count > 1 else "is",
                cur_page=index + 1,
                max_page=maximum
            )
            reply += list_message
            bot.reply(reply, message)
        except IndexError as e:
            bot.reply(str(e), message)


def print_owner(match, message, bot):
    bot.reply("{} is!".format(bot.adapters[message.adapter["_id"]].owner), message)

def always(val):
    def func(*args, **kwargs):
        return val
    return func

def check_telegram(match, message, bot):
    return message.adapter["adapter_type"] == TelegramAdapter.__name__

def register_with(argon):
    about_canned = argon.SOFTWARE_NAME + " " + argon.VERSION + "\n" + argon.DESCRIPTION
    if argon.AUTHORS: about_canned += " by " + argon.AUTHORS
    if argon.SOURCE_URL: about_canned += "\nSource code: " + argon.SOURCE_URL
    if argon.PRIVACY_POLICY_URL: about_canned += "\nPrivacy policy: " + argon.PRIVACY_POLICY_URL

    argon.add_commands(
        # About the bot
        CannedResponseCommand(RegexMatcher(r"!!about"),
                              "about",
                              "Show information about this bot.",
                              canned=about_canned
                              ),

        # Ping to test connection
        CannedResponseCommand(RegexMatcher(r"!!ping"),
                              "ping",
                              "Test the connection between the user and the bot.",
                              canned="Pong!"
                              ),

        # Echo a message
        Command(RegexMatcher(r"!!echo(?: (?P<message>.+))?"),
                "echo <message>",
                "Echo message.",
                lambda match, message, bot: bot.reply(match.group('message'), message) if match.group('message') else None
                ),

        # Help
        Command(RegexMatcher(r"!!help(?: ?(?:(?P<page>\d+)|(?P<command>.+)))?"),
                "help <page>|<command>",
                "Show help text for a page or command.",
                print_help
                ),

        # Print owner
        Command(RegexMatcher(r"!!who['’]s your owner\??"),
                "who's your owner?",
                "Show the owner of the bot",
                print_owner
                ),

        # Uptime
        Command(RegexMatcher(r"!!uptime"),
                "uptime",
                "Show how long the bot has been operating.",
                lambda match, message, bot: bot.reply(str(timedelta(seconds=time.time() - start_time)), message)
                ),

        # Is Mod
        Command(RegexMatcher(r"!!ismod"),
                "ismod",
                "Display if you can access admin commands on Argon.",
                lambda match, message, bot: bot.reply(str(message.sent_by["is_mod"]), message)
                ),

        # respond to /start command that is automatically sent on Telegram
        CannedResponseCommand(StringMatcher("/start"),
                              "start",
                              "Help function for startup in Telegram",
                              canned="To see the list of commands, send '!help' or '/help'.",
                              display_condition=always(False), exec_condition=check_telegram
                              ),
    )
