import requests


# Servers may become inaccessible anytime.
# Check public SearX instances in https://github.com/asciimoo/searx/wiki/Searx-instances or https://stats.searx.xyz/.
# Servers also may not offer JSON result or block/limit automated request. Check with this script.

searxes = ["https://trovu.komun.org/",       "https://searx.ca/",               "https://searx.canox.net/", 
           "https://searx.ch/",              "https://searx.drakonix.net/",     "https://wtf.roflcopter.fr/searx/",
           "https://searchx.mobi/",          "https://timdor.noip.me/searx/",   "https://srx.sx/",
           "https://ai.deafpray.wtf/searx/", "https://anonyk.com/",             "https://beezboo.com/",
           "https://burtrum.org/searx/",     "https://dynabyte.ca/",            "https://unmonito.red/",
           "https://framabee.org/",          "https://goso.ga/",                "https://haku.ahmia.fi/",
           "https://huyo.me/",               "https://intelme.com/",            "https://jsearch.pw/",
           "https://le-dahut.com/searx/",    "https://mijisou.com/",            "https://suche.uferwerk.org/",
           "https://openworlds.info/",       "https://perfectpixel.de/searx/",  "https://rapu.nz/",
           "https://roflcopter.fr/",         "https://s.cmd.gg/",               "https://search.activemail.de/", 
           "https://search.anonymize.com/",  "https://search.azkware.net/",     "https://search.datensturm.net/", 
           "https://search.ethibox.fr/",     "https://search.fossdaily.xyz/",  "https://seeks.hsbp.org/",
           "https://search.gibberfish.org/", "https://search.lgbtq.cool/",      "https://search.mdosch.de/", 
           "https://search.modalogi.com/",   "https://search.moravit.com/",     "https://search.paulla.asso.fr/", 
           "https://search.poal.co/",        "https://search.seds.nl/",         "https://search.snopyta.org/", 
           "https://search.spaeth.me/",      "https://search.st8.at/",          "https://search.stinpriza.org/", 
           "https://search.sudo-i.net/",     "https://search.tolstoevsky.ml/",  "https://searx.anongoth.pl/",
           "https://searx.cybt.de/",         "https://searx.dnswarden.com/",    "https://searx.li/",
           "https://searx.elukerio.org/",    "https://searx.everdot.org/",      "https://searx.foo.li/", 
           "https://searx.fr32k.de/",        "https://searx.good.one.pl/",      "https://searx.laquadrature.net/",
           "https://searx.gotrust.de/",      "https://searx.hardwired.link/",   "https://searx.itunix.eu/",
           "https://searx.libmail.eu/",      "https://searx.lynnesbian.space/", "https://spot.ecloud.global/",
           "https://searx.nakhan.net/",      "https://searx.nixnet.xyz/",       "https://searx.nnto.net/", 
           "https://searx.openhoofd.nl/",    "https://searx.openpandora.org/",  "https://searx.operationtulip.com/", 
           "https://searx.orcadian.net/",    "https://searx.pofilo.fr/",        "https://searx.prvcy.eu/", 
           "https://searx.pwoss.xyz/",       "https://searx.remote-shell.net/", "https://searx.ro/", 
           "https://searx.ru/",              "https://searx.site/",             "https://searx.solusar.de/", 
           "https://searx.targaryen.house/", "https://searx.tuxcloud.net/",     "https://searx.wegeeks.win/", 
           "https://searx.win/",             "https://searx.zareldyn.net/",     "https://searx.zdechov.net/",
           "https://suche.ftp.sh/"]

params = {}
params['q'] = 'searx'
params['format'] = 'json'

for server in searxes:
    response = None
    try:
        response = requests.get("{}?".format(server if server[-1]=='/' else server+"/"), params=params, headers = {'User-agent': 'ArgonBot 2.0'})
        results = response.json()
    except Exception as e:
        print("Testing "+server)
        if response is None:
            print(e)
            continue
        print(response)
        if response.status_code is 200:
            print(e)
            continue
        print(response.text)
