from datetime import datetime
import threading, subprocess, os, platform
from argon.adapter import Adapter
from argon.message import Message
from argon.type import MessageType, EventType

class ConsoleAdapter(Adapter):
    def __init__(self, nick="Argon"):
        super(ConsoleAdapter, self).__init__()
        self.nick = nick
        self.message_id = 0
        self.capabilities = {MessageType.PLAINTEXT}

        threading.Thread.__init__(self)

    def run(self):
        try:
            while True:
                print('>', end=' ')
                input_str = input()
                self.message_id += 1
                flags = []
                if "@" + self.nick in input_str:
                    flags += EventType.ARGON_MENTIONED
                message = Message([(input_str, MessageType.PLAINTEXT)],
                                   {"user_id": "console user", "is_mod": True},
                                   "console",
                                   datetime.now(),
                                   {"_id": self._id, "ident": self.identifier, "adapter_type": self.__class__.__name__},
                                   self.message_id,
                                   flags)

                self.callback(message)
        except EOFError:
            print("^D\nGoodbye.")
            pass

    def register_callback(self, func, _id):
        self.callback = func
        self._id = _id

    def send(self, message, _):
        for line in message.split("\n"):
            msg = line.strip(" ")
            print('< {msg}'.format(msg=msg))

    def reply(self, message, _):
        self.send(message, None)

    def send_media(self, msgtype: MessageType, location: str, group = None, title = "", reply_to = None):
        # This command will open the file with default OS application.
        # Filetypes are handled by the OS, so there's no need to check for MessageType.
        if platform.system() == 'Darwin':       # macOS
            subprocess.call(('open', location))
        elif platform.system() == 'Windows':    # Windows
            os.startfile(location)
        else:                                   # linux variants
            subprocess.call(('xdg-open', location))

        if title:
            self.send(title, group)
