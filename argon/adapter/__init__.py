import threading
from datetime import datetime
from argon.type import MessageType
from argon.constants import VERSION


class Adapter(threading.Thread):
    def __init__(self, identifier="!", require_mention=False):
        self.identifier = identifier
        self.identifier_optional = "(?:" + self.identifier + ")?"
        self.owner = None
        self.capabilities = []
        self.startup_errors = []
        self.require_mention = require_mention # If true, adapter will ignore messages that does not mention this bot.

    def _on_initialized(self, to):
        """Adapter should call this function after it has successfully connected to the server
        and is ready to operate."""

        self.send("I'm awake! Version {}".format(VERSION), to)
        self.send("Startup time: " + datetime.now().isoformat(), to)
        # Report startup errors to owner
        if self.startup_errors:
            self.send("Startup errors:", to)
        else:
            self.send("All things normal!", to)
        for err in self.startup_errors:
            self.send("• {}".format(err), to)

    def register_callback(self, func, _id):
        """This function is called by Argon to register message processing function to adapters.
        func: message evaluation function. Message should be formatted and passed onto this function.
        _id: unique ID used to refer to this specific instance."""
        pass

    def send(self, message, group):
        """Send plain text message to 'group'."""
        pass

    def send_media(self, msgtype: MessageType, location: str, group = None, title = "", reply_to = None):
        """Send message that includes media
        Arguments:
            type: media type, can be 3X.
            location: file location, either local or online.
            group: group to send the message; optional
            title: explanation of the media. can be sent as a single message (if supported) or as separate messages; optional
            reply_to: message to reply to; optional"""

        pass
    def reply(self, message, reply_to):
        """Send a reply to message.
        reply_to: message to reply to.
        use the information in this Message object to determine the recepient and the best format for reply."""
        pass

