import sys
import re
from argon import plugin_loader
import argon.constants as constants

# Commands
from argon.command import Command
# Adapters
from argon.adapter import Adapter
from argon.adapter.telegram import TelegramAdapter

from argon.type import MessageType

_MAGIC_SRC_URL_VALUE = "source provided in another way"


class Argon:
    def __init__(self, adapters, source_url, privacy_policy_url=None, commands=None):
        if not source_url:
            raise ValueError(
                "Since this bot is released under the AGPL, you must inform users where they can "
                "find the source code for your version (including your modifications). There's a "
                "built-in mechanism for this, which you are encouraged to use. See the "
                "Configuration section of the README. To disable this check, you can set the URL "
                'to the exact value "{}" (without the quotes).'.format(_MAGIC_SRC_URL_VALUE)
            )
        self.SOURCE_URL = None if source_url == _MAGIC_SRC_URL_VALUE else source_url
        self.PRIVACY_POLICY_URL = privacy_policy_url
        self.SOFTWARE_NAME = constants.SOFTWARE_NAME
        self.VERSION = constants.VERSION
        self.DESCRIPTION = constants.DESCRIPTION
        self.AUTHORS = constants.AUTHORS

        self.commands = commands if commands is not None else []
        self.metadata = dict()
        self.adapters = dict()
        self.flags = list()

        self.add_adapters(adapters)

    def add_adapters(self, adapters: dict):
        for _id, adapter in adapters.items():
            self.adapters[_id] = adapter
            self.adapters[_id].register_callback(self.process, _id)

    def add_commands(self, *commands):
        self.commands += commands

    def run(self):
        for adapter in self.adapters.values():
            adapter.start()

    def process(self, message):
        found_match = False

        message.flags += self.flags

        for command in self.commands:
            if command.eval(message, self) is not None:
                found_match = True

    def send_media (self, msgtype, location, _id, group = None, title = "", reply_to = None):
        self.adapters[_id].send_media(msgtype, location, group, title, reply_to)

    def send(self, message, group, _id):
        self.adapters[_id].send(message, group)

    def reply(self, message, reply_to):
        self.adapters[reply_to.adapter["_id"]].reply(message, reply_to)

    def finalise(self):
        for adapter in self.adapters.values():
            adapter.finalise()
