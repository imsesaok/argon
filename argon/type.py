from enum import Enum


class Type(Enum):
    pass


class MessageType(Type):
    """Enum with code for types of message Argon can process.

    Each adapter is encouraged(but not required) to broadcast these types of messages if supported by the protocol.

    If the target protocol does not support certain types or if it supports content types outside of this list,
     implement it with the closest sensible type.

    For example, link can be sent as PLAINTEXT, and images with text can be sent as a combination of
     IMAGE and PLAINTEXT with same id.
    """

    UNKNOWN = -1

    # Text Code: 1X
    PLAINTEXT = 10
    RICH_TEXT = 11 #Unidentified type of rich text
    MARKDOWN = 12 #Any Markdown-esque text
    HTML = 15

    # Binary(file) Code: 3X
    # Types such as Audio are included only if they have dedicated support.
    BINARY = 30
    AUDIO = 31
    IMAGE = 33
    VIDEO = 34
    STICKER = 36 #Set of stock images with unique identifier

    # Binary Stream Code: 4X
    BINARY_STREAM = 40
    AUDIO_STREAM = 41
    VIDEO_STREAM = 43


class EventType(Type):
    """Enum with code for types of events Argon can process.

    Each adapter is encouraged(but not required) to broadcast these types of events if supported by the protocol.

    If the target protocol does not support certain types or if it supports content types outside of this list,
     implement it with the closest sensible type.

    For example, when a user changes "mood", you can either send USER_INFORMATION_CHANGED or USER_DESCRIPTION_CHANGED,
     with additional information.

    If target platform does not distinguish user types, use code for users.
    Code named ARGON is only used for operation that takes place regarding Argon, i.e., sender.
    """
    UNKNOWN = -3

    # Network / Adapter Operation Code: 1XX
    PING = 101
    CONNECTED = 130
    AUTH_REQUIRED = 141
    TIMEOUT = 142
    INVALID_CONTENT = 160
    CONTENT_TOO_LARGE = 162

    # Group(Room) Operation Code (Users): 3XX
    # 1X: User Join/Leave
    USER_JOIN = 311
    USER_INVITED = 312
    USER_LEAVE = 313
    USER_KICKED = 314

    # 2X: User Ability Restriction
    USER_GAIN_PRIVILEGE = 321
    USER_LOSE_PRIVILEGE = 322
    USER_MUTED = 325
    USER_UNMUTED = 326

    # 5X, 6X: User Status
    USER_AWAY = 351
    USER_OFFLINE = 352
    USER_ONLINE = 353
    USER_TYPING = 357

    USER_MENTIONED = 361

    # 7X: User Information
    USER_INFORMATION_CHANGED = 370
    USER_NAME_CHANGED = 371
    USER_DESCRIPTION_CHANGED = 373
    USER_PROFILE_CHANGED = 375

    # Group(Room) Operation Code (Bots): 4XX
    BOT_JOIN = 411
    BOT_INVITED = 412
    BOT_LEAVE = 413
    BOT_KICKED = 414

    BOT_GAIN_PRIVILEGE = 421
    BOT_LOSE_PRIVILEGE = 422
    BOT_MUTED = 425
    BOT_UNMUTED = 426

    BOT_OFFLINE = 452
    BOT_ONLINE = 453
    BOT_TYPING = 457

    BOT_MENTIONED = 461

    BOT_INFORMATION_CHANGED = 470
    BOT_NAME_CHANGED = 471
    BOT_PROFILE_CHANGED = 473

    # Group(Room) Operation Code (Argon): 5XX
    ARGON_JOIN = 511
    ARGON_INVITED = 512
    ARGON_LEAVE = 513
    ARGON_KICKED = 514

    ARGON_GAIN_PRIVILEGE = 521
    ARGON_LOSE_PRIVILEGE = 522
    ARGON_MUTED = 525
    ARGON_UNMUTED = 526

    ARGON_MENTIONED = 561

    # Miscellaneous Group(Room) Operation Code: 7XX
    GROUP_INFORMATION_CHANGE = 700
    GROUP_NAME_CHANGE = 701
    GROUP_DESCRIPTION_CHANGE = 702
    GROUP_IMAGE_CHANGE = 731

    # Message Operation Code: 9XX
    MESSAGE_EDITED = 911
    MESSAGE_REDACTED = 913
    MESSAGE_PRIVATE = 931

