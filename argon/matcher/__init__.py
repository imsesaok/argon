from typing import Optional, Any, Tuple
import re

from argon.message import Message
from argon.type import Type, MessageType


class Matcher:
    def match(self, message:Message, bot) -> Optional[Any]:
        """Matching Function.

        When there is a match, return the matching reference object,
        or object that contains match data (such as re.Match).
        When there is no match, return None.
        """
        pass

class AdapterMatcher(Matcher):
    def __init__(self, match_adapter:list):
        self.match_adapter = match_adapter

    def match(self, message, bot):
        pass


class TypeMatcher(Matcher):
    def __init__(self, match_type: list, strict_match = False):
        self.match_type = match_type
        self.strict_match = strict_match

    def match(self, message, bot) -> Optional[Tuple[Any, Type]]:
        for content, message_type in message.content:
            if message_type in self.match_type:
                return content, message_type  # Exact match

            if not self.strict_match:
                for match_type in self.match_type:
                    if match_type.value % 10 == 0 and message_type.value / 10 == match_type.value / 10:
                        return content, match_type  # Wildcard match for generic types (XX0)

        return None


class RegexMatcher(TypeMatcher):
    def __init__(self, regex):
        super(RegexMatcher, self)\
            .__init__(match_type=[MessageType.PLAINTEXT])
        self.regex = regex

    def match(self, message, bot) -> Optional[re.Match]:
        content, _ = super().match(message, bot)
        if content:
            regex = self.regex.replace("!!", message.adapter["ident"]).replace("!\!", "!!")
            return re.search("^" + regex + "$", content)
        return None


class StringMatcher(TypeMatcher):
    def __init__(self, string):
        super(StringMatcher, self)\
            .__init__(match_type=[MessageType.PLAINTEXT])
        self.string = string

    def match(self, message, bot) -> Optional[str]:
        content, _ = super().match(message, bot)
        return content if content == self.string else None
