import traceback

from argon.message import Message
from argon.matcher import Matcher


class Command:
    def __init__(self,
                 matcher: Matcher,
                 title: str,
                 description: str,
                 on_exec,
                 display_condition=lambda match, message, bot: True,
                 exec_condition=lambda match, message, bot: True,
                 flags=None):
        self.matcher = matcher
        self.title = title
        self.description = description
        self.on_exec = self.run_on_sandbox
        self.func = on_exec
        self.display_condition = display_condition
        self.exec_condition = exec_condition
        self.flags = flags if flags is not None else []

    def __str__(self):
        return '<Command "{}">'.format(self.title)

    def eval(self, message: Message, bot):
        match = self.matcher.match(message, bot)

        if match is not None:
            self.run_on_sandbox(match, message, bot)

        return match

    def test(self, message: Message, bot) -> bool:
        match = self.matcher.match(message, bot)
        if match:
            return self.exec_condition(match, message, bot)
        else:
            return False

    def run_on_sandbox(self, match, message, bot) -> None:
        try:
            if self.exec_condition(match, message, bot):
                self.func(match, message, bot)
        except Exception as e:
            traceback.print_exc()
            bot.send("Oops, something went horribly wrong! (%s)" % e, message.group, message.adapter["_id"])


class CannedResponseCommand(Command):
    def __init__(self,
                 matcher: Matcher,
                 title: str,
                 description: str,
                 canned="",
                 display_condition=lambda match, message, bot: True,
                 exec_condition=lambda match, message, bot: True,
                 flags=None):
        super(CannedResponseCommand, self).__init__(matcher, title, description, self.canned, display_condition,
                                                    exec_condition, flags)
        self.canned_response = canned

    def canned(self, match, message, bot):
        bot.reply(self.canned_response, message)
